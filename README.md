# OpenML dataset: Violent-Crime-by-Counties-in-Maryland

https://www.openml.org/d/43500

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Crime rates vary across space and time. The reasons crimes are committed in some places but not others can be difficult to detect because of complex socio-economic factors, but policymakers still need to understand how crime rates are changing from place to place and from time to time to inform their policies.

Many government statistics, such as crime rates, come from nested datasets. Most US States are divided into counties (Alaska has burrows, and Louisiana has parishes), and counties and county-level governments can vary within the same state. For example, one county might have a high population density and be urban, whereas a second county might have a low population density and be rural.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43500) of an [OpenML dataset](https://www.openml.org/d/43500). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43500/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43500/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43500/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

